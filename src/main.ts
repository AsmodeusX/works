import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'
import lazyPlugin from 'vue3-lazy'
// import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

createApp(App)
  .use(store)
  .use(router)
  .use(VueAxios, axios)
  .use(lazyPlugin, {
    loading: 'blank.gif',
    error: 'blank.gif'
  })
  // .use(BootstrapVue)
  // .use(BootstrapVueIcons)
  .mount('#app')
