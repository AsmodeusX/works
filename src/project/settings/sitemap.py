from django.contrib.sitemaps import Sitemap, GenericSitemap
from main.models import MainPageConfig


main = {
    'queryset': MainPageConfig.objects.all(),
    'date_field': 'updated',
}

sitemaps = {
    'main': GenericSitemap(main, changefreq='daily', priority=1),
}
