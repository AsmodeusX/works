from django.shortcuts import render_to_response
from django.views.generic import View
from django.utils.translation import ugettext_lazy as _
from main.models import MainPageConfig
from libs.json_return import json_success


class IndexView(View):
    def get(self, request, *args, **kwargs):
        config = MainPageConfig.get_solo()

        return json_success({
          'title': config.title,
        })
