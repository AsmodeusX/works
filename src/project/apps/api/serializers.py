from rest_framework import serializers
from works.models import Work


class WorksSerializer(serializers.ModelSerializer):
    preview_url = serializers.SerializerMethodField()
    company_name = serializers.SerializerMethodField()

    class Meta:
        model = Work
        fields = ('id', 'title', 'description', 'preview_url', 'slug', 'link', 'company_name', 'stage', 'role', 'tags')

    def get_preview_url(self, obj):
        request = self.context.get('request')
        preview_url = obj.preview.url

        return request.build_absolute_uri(preview_url)

    def get_company_name(self, obj):
        if obj.company:
            company_name = obj.company.name
        else:
            company_name = None

        return company_name
