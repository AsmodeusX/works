from django.urls import path
from . import api

app_name = 'api'
urlpatterns = [
  # path('blog/<id>/', api.BlogPostView.as_view(), name='blog_post'),
  path('works/filters/', api.WorksFiltersView.as_view(), name='blog'),
  path('works/', api.WorksView.as_view(), name='blog'),
]
