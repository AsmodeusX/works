from django.views.generic import View
from libs.json_return import json_success
from .serializers import WorksSerializer
from works.models import Work, Tag


class WorksView(View):
  def get(self, request, *args, **kwargs):
    works = Work.objects.all()
    serializer = WorksSerializer(works, context={
      'request': request,
    }, many=True)

    x = {
      'works': serializer.data,
    }

    return json_success(x)


class WorksFiltersView(View):
  def get(self, request, *args, **kwargs):

    x = {
      'roles': [{'id': role[0], 'val': role[1]} for role in Work.ROLE_CHOICES],
      'stages': [{'id': stage[0], 'val': stage[1]} for stage in Work.STAGE_CHOICES],
      'tags': [{'id': tag.id, 'val': tag.name} for tag in Tag.objects.all()]
    }

    return json_success(x)
