from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel
from ckeditor.fields import RichTextField
from django.shortcuts import resolve_url
from default_models.models import SEOPageConfig
from autoslug.fields import AutoSlugField
from adminsortable.models import SortableMixin
from sortedm2m.fields import SortedManyToManyField


class Company(models.Model):
    name = models.CharField(_('name'), max_length=64)
    link = models.URLField(_('link'), blank=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(_('name'), max_length=64)

    class Meta:
        verbose_name = _('Tag')
        verbose_name_plural = _('Tags')

    def __str__(self):
        return self.name


class Work(SEOPageConfig, SortableMixin):
    ROLE_CHOICES = (
        (0, _('Full')),
        (1, _('Frontend')),
        (2, _('Backend')),
        (3, _('Refinement of the Functional')),
    )

    STAGE_CHOICES = (
        (0, _('Master')),
        (1, _('Production')),
    )

    title = models.CharField(_('title'), max_length=64, )
    description = RichTextField(_('description'), blank=True, )
    preview = models.ImageField(_('preview'), )
    link = models.URLField(_('link'), )
    company = models.ForeignKey(Company, verbose_name=_('company'), on_delete=models.CASCADE, null=True, blank=True)
    slug = AutoSlugField(_('slug'), populate_from='title', editable=True)
    stage = models.PositiveSmallIntegerField(_('stage'), choices=STAGE_CHOICES, default=1)
    role = models.PositiveSmallIntegerField(_('role'), choices=ROLE_CHOICES, default=0)
    tags = SortedManyToManyField(Tag, verbose_name=_('tags'), related_name='tags', blank=True, sorted=False)
    sort_order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _('Work')
        verbose_name_plural = _('Works')
        ordering = ['sort_order']

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return resolve_url('works:work', slug=self.slug)
