from django.contrib import admin
from .models import Work, Company, Tag
from adminsortable2.admin import SortableAdminMixin


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name', 'link',
            ),
        }),
    )


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'name',
            ),
        }),
    )


@admin.register(Work)
class WorkAdmin(SortableAdminMixin, admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'title', 'description', 'preview', 'link', 'slug', 'stage', 'company', 'role', 'tags',
            ),
        }),
    )
    search_fields = ('title', )
    list_filter = ('stage', 'company', 'role', )
    list_display = ('title', 'link', 'stage', 'role', 'company', 'sort_order', )
    sortable_by = 'sort_order'
    ordering = ('sort_order', )
    prepopulated_fields = {
        'slug': ('title',),
    }
